package vipya_bhai;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class login {
	
	public login(WebDriver driver){
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(xpath="//input[@name='email']")
	private WebElement username;
	@FindBy(xpath="//input[@name='pass']")
	private WebElement password;
	
	
	public void user(String username) {
		this.username.sendKeys(username);
	}
	public void password(String password) {
		this.password.sendKeys(password);
	}
	
	

}
