package Base_pack;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

public class BaseClass {
	
	public static WebDriver driver;
	
	@BeforeClass
	@Parameters("browser")
	public static void setup(String browser) {
		if(browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver","D:\\MercuryTourDemo\\driver_exe\\chromedriver1.exe");
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
			
		}
		else if(browser.equals("firefox"))
		{
			System.setProperty("webdriver.gecko.driver","D:\\MercuryTourDemo\\driver_exe\\geckodriver.exe");
			driver=new FirefoxDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		}
		
	}

}
